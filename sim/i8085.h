#ifndef __I8085_H__
#define __I8085_H__
/***************************************************************************
    TYPE DEFINITIONS
***************************************************************************/

typedef struct _i8085_state i8085_state;
struct _i8085_state
{
	/*i8085_config		config;*/

	/*devcb_resolved_write8		out_status_func;
	devcb_resolved_write_line	out_inte_func;
	devcb_resolved_read_line	in_sid_func;
	devcb_resolved_write_line	out_sod_func;*/

	int 				cputype;		/* 0 8080, 1 8085A */
	PAIR				PC,SP,AF,BC,DE,HL,WZ;
	UINT8				HALT;
	UINT8				IM; 			/* interrupt mask (8085A only) */
	UINT8   			STATUS;			/* status word */

	UINT8				after_ei;		/* post-EI processing; starts at 2, check for ints at 0 */
	UINT8				nmi_state;		/* raw NMI line state */
	UINT8				irq_state[4];	/* raw IRQ line states */
	UINT8				trap_pending;	/* TRAP interrupt latched? */
	UINT8				trap_im_copy;	/* copy of IM register when TRAP was taken */
	UINT8				sod_state;		/* state of the SOD line */

	UINT8				ietemp;			/* import/export temp space */

	/*device_irq_callback	irq_callback;*/
	/*legacy_cpu_device *device;*/
	UINT16 (*irq_callback)(void *dev, UINT32 mask);
	void *device;
	address_space *program;
	/*address_space *direct;*/
	address_space *io;
	int icount;
};

extern void i808x_cpu_execute(i8085_state *cpustate);
extern void cpu_init_i8080(i8085_state *state);
extern void cpu_init_i8085(i8085_state *state);
extern void i808x_cpu_reset(i8085_state *cpustate);
extern void i808x_set_irq_line(i8085_state *cpustate, int irqline, int state);


/***************************************************************************
    CONSTANTS
***************************************************************************/

enum
{
	I8085_PC, I8085_SP, I8085_AF, I8085_BC, I8085_DE, I8085_HL,
	I8085_A, I8085_B, I8085_C, I8085_D, I8085_E, I8085_F, I8085_H, I8085_L,
	I8085_STATUS, I8085_SOD, I8085_SID, I8085_INTE,
	I8085_HALT, I8085_IM,

	I8085_GENPC/* = STATE_GENPC*/,
	I8085_GENSP/* = STATE_GENSP*/,
	I8085_GENPCBASE/* = STATE_GENPCBASE*/
};

#define I8085_INTR_LINE     0
#define I8085_RST55_LINE	1
#define I8085_RST65_LINE	2
#define I8085_RST75_LINE	3

#define I8085_STATUS_INTA	0x01
#define I8085_STATUS_WO		0x02
#define I8085_STATUS_STACK	0x04
#define I8085_STATUS_HLTA	0x08
#define I8085_STATUS_OUT	0x10
#define I8085_STATUS_M1		0x20
#define I8085_STATUS_INP	0x40
#define I8085_STATUS_MEMR	0x80

/***************************************************************************
    TYPE DEFINITIONS
***************************************************************************/

//typedef struct _i8085_config i8085_config;
//struct _i8085_config
//{
//	devcb_write8		out_status_func;	/* STATUS changed callback */
//	devcb_write_line	out_inte_func;		/* INTE changed callback */
//	devcb_read_line		in_sid_func;		/* SID changed callback (8085A only) */
//	devcb_write_line	out_sod_func;		/* SOD changed callback (8085A only) */
//};
//#define I8085_CONFIG(name) const i8085_config (name) =

/***************************************************************************
    FUNCTION PROTOTYPES
***************************************************************************/
/*
DECLARE_LEGACY_CPU_DEVICE(I8080, i8080);
DECLARE_LEGACY_CPU_DEVICE(I8080A, i8080a);
DECLARE_LEGACY_CPU_DEVICE(I8085A, i8085);

CPU_DISASSEMBLE( i8085 );
*/
#define i8085_set_sid(cpu, sid)		cpu_set_reg(cpu, I8085_SID, sid)

#endif
