	;; YUMIMImips 8080-version 16-bit test program
	;; 2011 H.Tomari (public domain software)
	
	;; Memory-map configuration
lm_tail equ 0300h
pipe_p	equ 8000h
rank_p	equ 8004h
pct_p	equ 8008h
rct_p	equ 800ch
msg_p	equ 8100h
myrm1	equ 0300h
myrm2	equ 0380h
rm_p1	equ 4000h
rm_p2   equ 6000h
die_p	equ 8666h
	;; rom head
	org 0
_start:
	lxi SP,lm_tail		; stack init
	lhld rank_p		; (HL) <- my rank id
	mov a,h
	cpi 0
	jnz non0
	mov a,l
	cpi 0
	jnz non0
	;; this is rank 0
	jmp rcvdok
non0:				; this is not rank 0
	xchg
	lhld rct_p
	dcx h
	xchg 			; DE <- last rank id
	mov a,h
	cmp d
	jnz nonlst
	mov a,l
	cmp e
	jnz nonlst
	;; this is last rank
lastrnk:
	lda myrm1
	cpi 0
	jz lastrnk
lastrnk2:
	lda myrm2
	cpi 0
	jz lastrnk2
	mvi a,1
	sta msg_p
	sta die_p
	jmp $
nonlst:				; this is mid rank
	lda myrm1
	cpi 0
	jz nonlst
go2:
	lda myrm2
	cpi 0
	jz go2
rcvdok:	;; both RM ready
	mvi a,1
	sta rm_p1
	sta rm_p2
	jmp $
	
	END
