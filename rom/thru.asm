	;; YUMIMImips 8080-version test program
	;; 2011 H.Tomari (public domain software)
	
	;; configuration-specific params
lm_tail equ 0300h
pipe_p	equ 8000h
rank_p	equ 8004h
msg_p	equ 8100h
rm_size	equ 0100h
rmp1_h	equ 40h
rmp2_h	equ 60h
pid_w	equ 2 			; pipe id width (2 for 4 pipes)
rid_w  	equ 3			; rank id width (3 for 8 ranks)
lastrnk equ 7
	;; rom head
	org 0
_start:	
	lxi SP,lm_tail		; stack init
	xra a
	call _nextid		; b: my port
	mvi l,50h
	mov a,b
	cpi 0
	jnz addrok
	mvi l,00h
addrok:				; l: RM offset
	lda rank_p 		; if rank=0 then no wait
	cpi 0
	jz thisis0
	lda 300h
	mov b,a
	lda 350h
	ana b
	jz addrok
	;; copy 301h -> 4001h, 302h -> 4002h, ...
	lda rank_p
	cpi lastrnk
	jz thisisend
thisis0:
	mvi a,1
	mvi h,rmp1_h
	mov m,a
	mvi h,rmp2_h
	mov m,a
	sta msg_p
	jmp $
thisisend:
	mvi a,66h
	sta msg_p
	jmp $
	
	;; nextid: calc next pipe id. a=0: upper port, a=1: lower port
	;; return: a=next pipe id. b=next port(0:upper or 1:lower)
_nextid:
	mov c,a
	mvi a,pid_w
	sui 1
	mov b,a 		; b: pid_w-1 (shift amount)
	mov a,c			; a: port id
	;; first shift A to p-width to make 0000APP
	mvi d,1
nxtid_l0:
	rlc
	mov c,a
	mov a,b			; b: counter
	sub d
	mov b,a
	mov a,c			; a: 00a000...
	jnz nxtid_l0
	;; shift right pipe id
	mov b,a 		; b: portid moved to msb
	lda pipe_p
	rrc
	ani 7fh
	ora b 			; a: next pipe id
	mov c,a
	lda pipe_p
	ani 01h
	mov b,a
	mov a,c
	ret
	
	END
