	;; YUMIMImips 8080-version 16-bit PDE
	;; 2011 H.Tomari (public domain software)
	
	;; Memory-map configuration
lm_tail equ 0300h
pipe_p	equ 8000h
rank_p	equ 8004h
pct_p	equ 8008h
rct_p	equ 800ch
pbwidth equ 8010h
msg_p	equ 8100h
myrm1	equ 0300h
myrm2	equ 0380h
rm_p1	equ 4000h
rm_p2   equ 6000h
die_p	equ 8666h
	;; rom head
	org 0
_start:
	lxi SP,lm_tail		; stack init
	mvi a,0
	lxi h,myrm1
	mov m,a
	inx h
	mov m,a
	lhld rank_p		; (HL) <- my rank id
	mov a,h
	cpi 0
	jnz non0
	mov a,l
	cpi 0
	jnz non0
	;; this is rank 0
	mvi a,3
	lxi h,skb
	mov m,a
	call _calc
	call _ports_done
	;sta msg_p
	jmp $
non0:				; this is not rank 0
	xchg
	lhld rct_p
	dcx h
	xchg 			; DE <- last rank id
	mov a,h
	cmp d
	jnz nonlst
	mov a,l
	cmp e
	jnz nonlst
	;; this is last rank
lastrnk:
	lda myrm1
	cpi 0
	jz lastrnk
lastrnk2:
	lda myrm2
	cpi 0
	jz lastrnk2
	mvi a,1
	sta die_p
	xra a
	sta myrm1
	sta myrm2
	jmp lastrnk
nonlst:				; this is mid rank
	xra a
	sta myrm1
	sta myrm2
	call _mainloop
	mvi a,2
	sta msg_p
	call _calc
	call _ports_done
	mvi a,4
	sta msg_p
	jmp nonlst
	
_calc:
	lxi h,skb
	mov a,m
	cpi 3
	rc
	;sta msg_p
	lda pbwidth
	dcr a
	mov m,a
	lxi h,here
	mvi c,0
	mov d,c
calclo:
	mov a,m
	rlc 			; a <- a+a
	push h			;
	lxi h,minus
	dad b
	add m
	lxi h,plus
	dad b
	add m
	pop h			;
	rrc
	rrc
	mov m,a
	inr c
	inx h
	mov a,c
	cpi 4
	jc calclo		; loop end

	mvi a,3
	sta msg_p
	lhld pipe_p
	mvi c,0
	call build_skb

	lhld pipe_p
	inx h
	mvi c,1
	call build_skb

	lhld pipe_p
	dcx h
	mvi c,-1
	call build_skb

	xra a
	sta skb
	
	ret

build_skb:			; HL: dest c:-1/0/+1
	xchg
	lhld pct_p
	dcx h
	mov a,h
	ana d
	mov d,a
	mov a,l
	ana e
	mov e,a
	lxi h,skb
	push h
	inx h
	mov m,e
	inx h
	mov m,d
	inx h
	mov m,c
	pop h
	mvi a,1
	ana e
	call _append
	ret
	
	;;;;; lib ;;;;;
_ports_done:
	mvi a,1
	sta rm_p1
	sta rm_p2
	ret
_mainloop:
	mvi b,0
	lxi h,rm_p1
	call _openport
	call _zeroport
	lxi h,rm_p2
	call _openport
	call _zeroport
	lxi h,myrm1
	call _handle_ip
	lxi h,myrm2
	call _handle_ip
	ret
	;; handle_ip
	;; HL: input port pointer
_handle_ip:
	push h
	mvi b,1
	call _openport
	mvi a,1
	sta msg_p
	inx h
	xra a 			; init counter
hndlip_l1:
	mov b,m 		; load n [BAR+1]
	cmp b
	
	mov c,a
	mov a,b
	mov a,c
	
	jz hndlip_l1_o
	push psw
	; debug
	;sta msg_p
	;mov a,h
	;sta msg_p
	;mov a,l
	;sta msg_p
	;mov a,b
	;sta msg_p
	;pop psw
	;push psw
	; end debug
	push h
	inx h
	; calc addr.
	rlc
	rlc
	rlc
	mov c,a
	mvi b,0
	dad b			; HL: packet head
	mov a,m 		; A: #
	cpi 0
	jz handle_here
	mov b,a
	lda pbwidth
	sub b
	push h
	inx h
	; here comes the routing code a:number
	mvi c,8
	cmp c
	jc b_in_lower
	sub c
	inx h
b_in_lower:
	mov b,m
	mvi d,1 		; hot-bit
rotl0:
	cpi 0
	jz rotend
	mov c,a
	mov a,d
	rlc
	mov d,a
	mov a,c
	dcr a
	jmp rotl0
rotend:
	pop h 			; back to packet head
	mov a,m
	dcr a
	mov m,a
	mov a,b
	ana d 			; a: port
	call _append
end_handle:
	pop h
	pop psw
	inr a
	jmp hndlip_l1
hndlip_l1_o:
	pop h
	xra a 	; change sync status here
	mov m,a
	ret
handle_here:
	; here comes the PDE code
	; HL: packet head
	push psw
	push h
	
	inx h
	inx h
	inx h 			; HL <- payload head
	
	mov a,m
	xchg
	cpi 0
	jc cpy2minus
	jz cpy2here
	; else here plus
	lxi h,plus
paramsok:
	xchg
	mvi a,4
	call bcopy
	lxi h,skb
	mov a,m
	inr a
	mov m,a
	pop h
	pop psw
	jmp end_handle
cpy2minus:
	lxi h,minus
	jmp paramsok
cpy2here:
	lxi h,here
	jmp paramsok
	
	;; open port hl: port pointer
	;; b: wait until port status is b
_openport:
	mov a,m
	cmp b
	jnz _openport
	;sta msg_p
	ret
_zeroport:
	inx h
	xra a
	mov m,a
	ret
	;; _append
	;; a>0: append to port 1 else 0
	;; HL: pointer to 8-byte packet
_append:
	xchg 			; DE: src ptr
	lxi h,rm_p1
	cpi 0
	jz append_go
	lxi h,rm_p2		; HL: target port pointer
append_go:
	inx h
	mov a,m
	inr a
	mov m,a
	dcr a
	inx h
	rlc
	rlc
	rlc
	mov c,a
	xra a
	mov b,a
	dad b 			; HL: destination ptr
	xchg
	mvi a,8 		; copy 8 bytes from (HL) -> (DE)
	;call bcopy
	;ret
	;; bcopy HL -> DE size: a [bytes]
bcopy:
	mov b,m

	;mov e,a
	;mov a,b
	;sta msg_p
	;mov a,e
	
	xchg
	mov m,b
	xchg
	inx d
	inx h
	dcr a
	cpi 0
	jnz bcopy
	ret
	;; .data
minus:
	db 1
	db 2
	db 3
	db 4
plus:
	db 5
	db 6
	db 7
	db 8
skb:
	db 0
	db 0
	db 0
	db 0
here:
	db 9
	db 10
	db 11
	db 12
	
	END
